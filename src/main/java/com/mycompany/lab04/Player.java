/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

/**
 *
 * @author fewxi
 */
public class Player {

    private char symbol;
    private int Wincount, Losecount, Drawcount;

    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getWincount() {
        return Wincount;
    }

    public int getLosecount() {
        return Losecount;
    }

    public int getDrawcount() {
        return Drawcount;
    }

    public void win() {
        Wincount++;
    }

    public void lose() {
        Losecount++;
    }

    public void draw() {
        Drawcount++;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", Wincount=" + Wincount + ", Losecount=" + Losecount + ", Drawcount=" + Drawcount + '}';
    }
    
}
